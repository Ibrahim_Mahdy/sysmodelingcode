/**
 * @(#) doctorCalenderADO.java
 */

package DomainModel.applicationModel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;


public class doctorCalenderADO
{

	public static int getavailableOncologist( Date appointment ) {

		// for test purposes, we print a set of available ONcologists for user to choose from and return the proID
		// of selected doctor to be saved in visit table.
		
		// in actual implementation this will select all available oncologists for the provided date and return chosen one
		// if there are no available doctors we return 0 and inform user that there are no available onclogists for this appointment.
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please choose Oncologist: ");
		System.out.println("1. DR. Magdy");
		System.out.println("2. DR. Sara");
		System.out.println("3. DR. Faloya");
		System.out.println("4. DR. Galya");
		
		int profId = Integer.parseInt(scanner.nextLine()); 
		
		return profId;
	}

	
}
