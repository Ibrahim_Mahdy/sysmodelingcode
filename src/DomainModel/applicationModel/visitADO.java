/**
 * @(#) visitADO.java
 */

package DomainModel.applicationModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.xml.crypto.Data;

public class visitADO
{
	public static Boolean checkIfappointmentAvailable( String unnamed1 )
	{
		return null;
	}
	
	public static String insertVisit( visit bookedVisit )
	{
		Connection connection = DatabaseManager.getDbConnection();
		if (connection == null) {
			return "Database Connection Error!";
		}
		try {
			Statement statement = connection.createStatement();
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateString = format.format( bookedVisit.getBookedDateTime()  );
			
			statement.execute("INSERT INTO visit(patientID,bookedDateTime,specialistID) VALUES('" + bookedVisit.getPatientID() + "','" + dateString + "'," + bookedVisit.getSpecialistID() + ")");
			statement.close();
			connection.close();
			return "visit booked successfully";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	
}
