/**
 * @(#) Controller.java
 */

package DomainModel.applicationModel;

import java.util.Date;

public class Controller
{
	public static String registerMember( String userName, String password, String role )
	{
		if(hMemberADO.memberExist(userName)) {
			return("User already exists!");	
		}
		else {
			hospitalMember member = new hospitalMember(userName, password, role);		
			return(hMemberADO.insertMember(member));	
		}
	}
	

	public static String login( String userName, String password )
	{
		if(hMemberADO.verifyLogin(userName, password)) {
			return("Logged in successfully!");
		}
		else {
			return("Invalide credenitals!");
		}

	}
	
	public static String registerseniorOncologist( int profId, String name, String surname, String level, String type)
	{
		if(OncologistADO.oncologistExists(profId)) {
			return("Oncologist with same ID already exists!");	
		}
		else if (level == "Senior") {
			senior seniorOnco = new senior( name,  surname,  profId,  level,  type);		
			return(OncologistADO.insertSenior(seniorOnco));	
		}
		else if(level == "Junior") {
			junior juniorOnco = new junior( name,  surname,  profId,  level,  type);		
			return(OncologistADO.insertJunior(juniorOnco));	
		}
		return type;
	}
	
	
	public static String booknextVisit(String patientID, Date bookedDateTime)
	{
		int doctorID = doctorCalenderADO.getavailableOncologist(bookedDateTime);
		if(doctorID == 0) {return "No Oncologists available fot this appointment!";}
		else {
			visit vv = new visit(patientID, bookedDateTime,doctorID);
			
			return(visitADO.insertVisit(vv));
		}		
	}

	public static String removePatient( String codeID )
	{
		return(patientADO.deletePatient(codeID));
	}
	
	

	
	
}
