/**
 * @(#) patientADO.java
 */

package DomainModel.applicationModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class patientADO
{

	public static String deletePatient( String codeID )
	{
		Connection connection = DatabaseManager.getDbConnection();
		if (connection == null) {
			return "Database Connection Error!";
		}
		try {
			Statement statement = connection.createStatement();
			statement.execute("DELETE FROM patient where codeID = '" + codeID + "'");
			statement.execute("DELETE FROM visit where patientID = '" + codeID + "'");
			statement.close();
			connection.close();
			return "Patient deleted successfully";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "";
		
	}
}
	

