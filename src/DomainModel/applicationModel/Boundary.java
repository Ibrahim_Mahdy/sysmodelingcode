/**
 * @(#) Boundary.java
 */

package DomainModel.applicationModel;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;


import DomainModel.applicationModel.*;

public class Boundary {
	public static void main( String[] args ) {
		System.out.println("Do you want to initialize the database? (Y/N)");
		Scanner scan = new Scanner(System.in);
		if (scan.nextLine().equalsIgnoreCase("Y")) {
			
			if (! DatabaseManager.resetDatabase()) {
				System.out.println("Error resetting database!");
				System.exit(-1);
			} 
									
		}
		while (true) {
			 
			System.out.println("Select an option:");
			System.out.println("1. register as a user");
			System.out.println("2. Login into your account");
			System.out.println("0. Exit");

			int choice = Integer.parseInt(scan.nextLine());
			if (choice == 1) {
				register();
			} else if (choice == 2){				
				login();		
			}			
			else {
				System.exit(-1);
				break;
			}
		}
	}
	
	
	
	private static void register() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter username");
		String username = scanner.nextLine();
		System.out.println("Please enter password");
		String password = scanner.nextLine();
		System.out.println("Please enter role: (admin or oncologist or surgeon)");
		String role = scanner.nextLine();

		System.out.println(Controller.registerMember(username,password,role));
		
		Options();
	}
	
	
	private static void login() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter your username");
		String username = scanner.nextLine();
		System.out.println("Please enter your password");
		String password = scanner.nextLine();
		
		String logged = Controller.login(username, password);
		System.out.println(logged);
		if(logged == "Logged in successfully!") {Options();}
			
	}
	
	private static void Options() {
		String role = hospitalMember.getRole().toLowerCase();
		System.out.println();
		switch (role) {
			case "admin":
				
				Scanner scanner = new Scanner(System.in);
				System.out.println("1. Add a new oncologist");			
				System.out.println("2. Add visit"); //books the follow-up visit use case
				System.out.println("3. Delete patient");				
				System.out.println("4. Exit");
				int option = Integer.parseInt(scanner.nextLine());
				switch (option) {
					case 1:
						addOncologist();
						break;
					case 2:
						addVisit();
						break;
					case 3:
						removePatientbyID();
						break;					
					case 4:
						System.exit(-1);
						break;
				}			
				
				break;
			case "oncologist":
				System.out.println("You have no power here!");
			case "surgeon":
				System.out.println("You have no power here!");			
				break;
		}
	}
	

	
	private static void addOncologist() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Professional ID: (Integer)");
		int profId = Integer.parseInt(scanner.nextLine());	
		System.out.println("Oncologist first name: ");
		String name = scanner.nextLine();
		System.out.println("Oncologist last name:");
		String surname = scanner.nextLine();	
		String type = getType(scanner, "oncologist", new String[]{"Medical oncologist", "Radiation oncologist", "Surgical oncologist", "Gynecologic oncologist", "Pediatric oncologist", "Hematologist-oncologist"});
		String level = oncologistLevel(scanner);
		System.out.println(Controller.registerseniorOncologist(profId, name, surname, level, type));
		Options();
	}
	
	
	private static String getType(Scanner scanner, String type, String[] types) {
		System.out.println(String.format("What's the type of the %s?. Choose one.", type));
		for (int i = 0; i < types.length; i++) {
			System.out.println((i + 1) + "." + types[i]);
		}
		int choice = Integer.parseInt(scanner.nextLine());
		if (choice < 1 || choice > types.length) {
			return getType(scanner, type, types);
		}
		return types[choice - 1];
	}
	

	private static String oncologistLevel(Scanner scanner) {
		System.out.println("Choose Level of oncologist: ");
		System.out.println("1.Junior");
		System.out.println("2.Senior");
		System.out.println("3.Exit");
		int choice = Integer.parseInt(scanner.nextLine());
		if (choice == 1) {
			return "Junior";
		} else if (choice == 2) {
			return "Senior";
		} else {
			System.exit(-1);
		} 
		return oncologistLevel(scanner);
	}
	
	
	private static void addVisit() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter visit dateTime in format YYYY-MM-DD HH:mm:ss");
		String appointment = scanner.nextLine();
		java.util.Date date1 = null;
		
		System.out.println("Please enter paient ID code");
		String patientCode = scanner.nextLine();
		

		try {
			date1 = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss").parse(appointment);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		System.out.println(Controller.booknextVisit(patientCode, date1));
	}
	
	private static void removePatientbyID() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter patient codeID");
		String codeID = scanner.nextLine();
		System.out.println(Controller.removePatient(codeID));
	}
	
}