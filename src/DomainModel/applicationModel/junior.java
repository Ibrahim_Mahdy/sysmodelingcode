/**
 * @(#) junior.java
 */

package DomainModel.applicationModel;

public class junior extends oncologist
{
	public enum juniorType {
		Medical_Student,
		Physician_Assistant
	}
	

	private String level;
	private String type;
	
	public junior(String name, String surname, int profID, String level, String type) {
		this.firstName = name;
		this.surName = surname;
		this.profesionalID = profID;
		this.level = level;
		this.type = type;
	}

	public String getName() {
		return firstName;
	}

	public void setName(String name) {
		this.firstName = name;
	}

	public String getSurname() {
		return surName;
	}

	public void setSurname(String surName) {
		this.surName = surName;
	}

	public int getProf_id() {
		return profesionalID;
	}

	public void setProf_id(int proID) {
		this.profesionalID = proID;
	}

	public String getlevel() {
		return level;
	}

	public void setlevel(String level) {
		this.level = level;
	}

	public String gettype() {
		return type;
	}

	public void settype(String type) {
		this.type = type;
	}
	
	
}
