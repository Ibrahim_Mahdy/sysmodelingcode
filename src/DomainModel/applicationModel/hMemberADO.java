/**
 * @(#) hMemberADO.java
 */

package DomainModel.applicationModel;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class hMemberADO
{

	public static Boolean memberExist( String userName )
	{
		Connection connection = DatabaseManager.getDbConnection();
		if (connection == null) {
			return true;
		}
		
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM hospitalMembers WHERE userName = '" + userName + "'");
			boolean useExists = resultSet.next();
			resultSet.close();
			connection.close();
			connection.close();
			return useExists;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
	
	
	public static String insertMember( hospitalMember member )
	{

		Connection connection = DatabaseManager.getDbConnection();
		if (connection == null) {
			return "Database Connection Error!";
		}
		try {
			Statement statement = connection.createStatement();
			statement.execute("INSERT INTO hospitalMembers(userName,password,role) VALUES('" + member.getUsername() + "','" + member.getPassword() + "','" + member.getRole() + "')");
			statement.close();
			connection.close();
			return "Registered successfully";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "";
		
	}
	
	
	
	public static Boolean verifyLogin( String userName, String password )
	{
		Connection connection = DatabaseManager.getDbConnection();
		if (connection == null) {
			return false;
		}
		
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM hospitalMembers WHERE userName = '" + userName + "' and password = '" + password + "'");
			boolean useExists = resultSet.next();
			if(useExists) {
				hospitalMember mem = new hospitalMember();
				mem.setRole(resultSet.getString("role"));
			}
			
			resultSet.close();
			connection.close();
			connection.close();
			return useExists;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	
}
