package DomainModel.applicationModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class DatabaseManager {

	public static Connection getDbConnection() {
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			return DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean resetDatabase() {

		try {
			Connection connection = getDbConnection();
			if (connection == null) {
				return false;
			}
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DROP TABLE IF EXISTS hospitalMembers");
			stmt.executeUpdate("DROP TABLE IF EXISTS oncologists");
			stmt.executeUpdate("DROP TABLE IF EXISTS visit");
			stmt.executeUpdate("DROP TABLE IF EXISTS surgeons");
			stmt.executeUpdate("DROP TABLE IF EXISTS patient");		
			stmt.executeUpdate("DROP TABLE IF EXISTS doctor");
			
			Statement statement = connection.createStatement();
			createhospitalMembers(statement);
			createOncologistTable(statement);
			createPatientTable(statement);
			createSurgeonsTable(statement);
			createVisitTable(statement);
			createdoctorCalenderTable(statement);
			return (statement.executeBatch().length > 0);

		
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return false;
	}

	private static void createhospitalMembers(Statement statement) throws SQLException {
		statement.addBatch("CREATE TABLE hospitalMembers (userName VARCHAR(150) NOT NULL,password VARCHAR(120) NOT NULL,role VARCHAR(120) NOT NULL, PRIMARY KEY (userName))");
	}

	private static void createOncologistTable(Statement statement) throws SQLException {
		statement.addBatch("CREATE TABLE oncologists (proID int NOT NULL, name VARCHAR(150) NOT NULL, surName VARCHAR(120) NOT NULL, type VARCHAR(120) NOT NULL, level VARCHAR(120) NOT NULL, PRIMARY KEY (proID))");
	}

	private static void createVisitTable(Statement statement) throws SQLException {
		statement.addBatch("CREATE TABLE visit (ID int NOT NULL IDENTITY, patientID VARCHAR(150) NOT NULL, bookedDateTime datetime NOT NULL, specialistID int NOT NULL, PRIMARY KEY (ID))");

	}
	
	private static void createPatientTable(Statement statement) throws SQLException {
		statement.addBatch("CREATE TABLE patient ( codeID VARCHAR(150) NOT NULL,isNational boolean NOT NULL, dateOfBirth date NOT NULL,insuranceCode VARCHAR(120) NOT NULL, privateCompany VARCHAR(120) NULL, specialistID int  NULL,PRIMARY KEY (codeID) )");
	}

	private static void createSurgeonsTable(Statement statement) throws SQLException {
		statement.addBatch("CREATE TABLE surgeons (proID int NOT NULL, name VARCHAR(150) NOT NULL, surName VARCHAR(120) NOT NULL, type VARCHAR(120) NOT NULL, PRIMARY KEY (proID))");

	}
	

	private static void createdoctorCalenderTable(Statement statement) throws SQLException {
		statement.addBatch("CREATE TABLE doctorCalender (ID int NOT NULL IDENTITY, proID int NOT NULL, appointment datetime NOT NULL, available boolean NOT NULL, PRIMARY KEY (ID))");
	}


}
