/**
 * @(#) OncologistADO.java
 */

package DomainModel.applicationModel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class OncologistADO
{
	public static Boolean oncologistExists( int profesionalID )
	{
		Connection connection = DatabaseManager.getDbConnection();
		if (connection == null) {
			return true;
		}
		
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM oncologists WHERE proID = " + profesionalID + "");
			boolean useExists = resultSet.next();
			resultSet.close();
			connection.close();
			connection.close();
			return useExists;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
	
	
	public static String insertSenior( senior oncologist )
	{

		Connection connection = DatabaseManager.getDbConnection();
		if (connection == null) {
			return "Database Connection Error!";
		}
		try {
			Statement statement = connection.createStatement();
			statement.execute("INSERT INTO oncologists(proID,name,surName,type,level) VALUES(" + oncologist.getProf_id() + ",'" + oncologist.getName()+ "','" + oncologist.getSurname() + "','" + oncologist.gettype() + "','" + oncologist.getlevel() + "')");
			statement.close();
			connection.close();
			return "oncologists registered successfully";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "";	
	}
	
	
	public static String insertJunior( junior oncologist )
	{

		Connection connection = DatabaseManager.getDbConnection();
		if (connection == null) {
			return "Database Connection Error!";
		}
		try {
			Statement statement = connection.createStatement();
			statement.execute("INSERT INTO oncologists(proID,name,surName,type,level) VALUES(" + oncologist.getProf_id() + ",'" + oncologist.getName()+ "','" + oncologist.getSurname() + "','" + oncologist.gettype() + "','" + oncologist.getlevel() + "')");
			statement.close();
			connection.close();
			return "oncologists registered successfully";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "";	
	}
}
