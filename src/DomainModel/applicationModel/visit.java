package DomainModel.applicationModel;

import java.util.Date;


public class visit {

	
	private String patientID;
	private Date bookedDateTime;
	private int specialistID;

	
	public visit(String patientID, Date bookedDateTime, int specialistID) {
		this.patientID = patientID;
		this.bookedDateTime = bookedDateTime;
		this.specialistID = specialistID;
	}

	
	public String getPatientID() {
		return patientID;
	}

	public void setPatientID(String pID) {
		this.patientID = pID;
	}

	public Date getBookedDateTime() {
		return bookedDateTime;
	}

	public void setBookedDateTime(Date dd) {
		this.bookedDateTime = dd;
	}

	public int getSpecialistID() {
		return specialistID;
	}

	public void setSpecialistID(int proID) {
		this.specialistID = proID;
	}
	
}
